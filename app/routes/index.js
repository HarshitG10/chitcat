'use strict';
const h = require('../helper');
module.exports = () => {
    let routes = {
        //all get methods
        'GET' : {
            '/': (req,res,next)=>{
                    res.render('login',{
                    'pageTittle' : 'My Login Page'
                 });
            },
            '/rooms': (req,res,next)=>{
                    res.render('rooms');
            },
            '/chat': (req,res,next)=>{
                    res.render('chatroom');
            },
            '/getsession': (req,res,next)=>{
                    res.send('My Favourite Color is : ' + req.session.favColor);
            },
            '/setsession': (req,res,next)=>{
                    req.session.favColor = 'Purple';
                    res.send('Session is Set');
                    
                    
            }
        },
        //All post methods
        'POST':{
        },
        'NA': (req,res,next)=>{
                    res.status(404).sendFile(process.cwd() + '/views/404.htm');
        }
    }
    //iteration thru all objects
    return h.route(routes);
}