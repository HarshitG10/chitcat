'use strict';
const router = require('express').Router();

let regrRoute = (r,m)=>{
    for(let k in r){
        //console.log(k);
        //console.log(typeof r[k] === 'object' && r[k] !== null && !(r[k] instanceof Array));
        if(typeof r[k] === 'object' & r[k] !== null & !(r[k] instanceof Array))
        {
            //console.log(r[k]," instanceof Array " + (r[k] instanceof Array));
            regrRoute(r[k],k);
        }else
        {
            if(m == 'GET')
            {
                router.get(k,r[k]); 
                //console.log( "method is " + m,'route function is ' + r[k]);
            }else if (m == 'POST'){
                router.post(k,r[k]);
                //console.log( m,r[k]);
            }else
            {
                router.use(r[k ]);
            }
        }
    }
}

let route = routes=>{
    regrRoute(routes);
    return router
}
module.exports ={
    route : route
}