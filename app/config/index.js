'use strict';
if(process.env.NODE_ENV === 'production'){
    module.exports = {
        host : process.env.host || "",
        dbURI : process.env.dbURI,
        secret: "ChatCatApp",
        fb:{
            clientID: process.env.fbClientId,
            clientSecret: process.env.fbClientSecret,
            callbackURL: process.env.host + "/auth/fb/callback",
            profileFields:  ["id","displayName","photos"]
        }
    }
}else
{
    module.exports = require('./development.json');
}