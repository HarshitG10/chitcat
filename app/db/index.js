'use strict';
const config = require('../config');
const Mongoose = require('mongoose').connect(config.dbURI);

Mongoose.connection.on('error',err=>{
    console.error(err);
});

module.exports = {
    Mongoose
}