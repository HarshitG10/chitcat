'use strict';
const Express = require('express');
const app = Express();
const chatCat = require('./app');
app.set('port',process.env.PORT || 22089);
app.use(Express.static('public'));
//when we set view engine to ejs express will 
//automatically import all the properties of ejs into system
app.set('view engine','ejs');


//we hav to set views property to the folder name in which we have kept .ejs files
//by default it is set to views only
// app.set('views','<your folder name>');
app.use('/',chatCat.router);
/*
app.get('/',(req,res,next)=>{
    //res.send('<h1>Hello Express!</h1>');
    //res.sendFile(__dirname + "/views/login.htm");
    
    // we will use render method when we will use EJS.
    //render methods will automatically looks into views folder for ejs files
    res.render('login',{
        'pageTittle' : 'My Login Page'
    });

});
*/
app.listen(app.get('port'),()=>{
    console.log("Server is running...");
});